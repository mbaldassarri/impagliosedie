function checkValue(value, tag) {
    if(value !== undefined && value !== null && value.trim().length){
        document.getElementById(tag).style.borderColor = "green";
        return true;
    }
    document.getElementById(tag).style.borderColor = "red";
    return false;    
};

function checkTel(value, tag) {
    if(!checkValue(value, tag)) { 
        return false;
    }
    var result = /^\d{5,10}$/.test(value);
    if(!result){
        document.getElementById(tag).style.borderColor = "red";
        return false;
    }
    document.getElementById(tag).style.borderColor = "green";
    return true;
};

function checkMail(value, tag) {
    if(!checkValue(value, tag)) {
        return false;
    }
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var result = re.test(value);
    if(!result){
        document.getElementById(tag).style.borderColor = "red";
        
        return false;
    }
    document.getElementById(tag).style.borderColor = "green";
    return true;
};



function checkForm(e) {
    var name = document.getElementById("name").value;
    var tel = document.getElementById("tel").value;
    var email = document.getElementById("email").value;
    var message = document.getElementById("text").value;
  
    var tagNome = "name";
    var tagTel = "tel";
    var tagMail = "email";
    var tagMsg = "text";


    checkValue(name, tagNome);
    checkValue(message, tagMsg);
    checkMail(email, tagMail);
    checkTel(tel, tagTel);
    if(checkValue(name, tagNome) && checkValue(message, tagMsg) && checkMail(email, tagMail) && checkTel(tel, tagTel)) {
        
        e.preventDefault();
   	$('#submitbt').prop("disabled",true);
   	$("#submitbt").prop('value', 'Inviato');
        
        // a sample AJAX request
        $.ajax({
        url : this.action,
        type : this.method,
        data : $(this).serialize(),
            success : function(response) {
                console.log("success");
            }
   	});
	
    
        return true;
    }

    e.preventDefault();
    return false;
    
    
};

window.onload = function() {
    
    var regform = document.getElementById("regForm");
    
    regform.onsubmit = checkForm;
   
};
