
function initMap() {
  var uluru = {lat: 43.1386431, lng: 13.6088874};
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 15,
    center: uluru
  });

  var contentString = '<div id="content">'+
      '<h1>Valter Iachini<h1>'+
      '<p>Via Tenna, 107 - 63832</p>'+
      '<p>Magliano di Tenna (FM)</p>'+
      '<p>Tel. 3334379888</p>'+
      '<p>valteriachini@yahoo.it</p>';
      

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });

  var marker = new google.maps.Marker({
    position: uluru,
    draggable: true,
    animation: google.maps.Animation.DROP,
    map: map,
    title: 'Posizione Falegnameria'
  });
  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });
}